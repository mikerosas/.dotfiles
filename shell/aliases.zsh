# Just screw the world
alias fnoff="sudo $HOME/k380-function-keys-conf/k380_conf -d /dev/hidraw3 -f off"
alias fnon="sudo $HOME/k380-function-keys-conf/k380_conf -d /dev/hidraw3 -f on"
#Preserve Environment on Sudo
alias fucking= "sudo "
alias package="apt-get"
#alias apt='apt-fast'
alias apt="sudo apt-get"
#alias vim="nvim"
alias vi="command vim"
alias ireallywantvi="command vi"
# Create parent directories on demand
alias mkdir="mkdir -pv"
# Colorize Diff Output.
alias diff="colordiff"
# Show open ports.
alias ports="netstat -tulanp"
# get web server headers #
alias header='curl -I'
# find out if remote server supports gzip / mod_deflate or not #
alias headerc='curl -I --compress'

alias g="git"
alias gitup="git checkout master && git pull"
alias be="bundle exec"

#Xubuntu XFCE shorcuts
alias fuckingshorcuts="xfconf-query -c xfce4-keyboard-shortcuts -l -v"
# Create a named tmux session
alias tm="tmux new -s"

# Create a tmux session named after the current directory
alias tmu="tmux new -s ${PWD##*/}"

alias ta="tmux attach"
alias tls="tmux ls"
alias tat="tmux attach -t"

# vim-like exit
alias :q="exit"
alias cl="clear"

# PS
alias psa="ps aux"
alias psg="ps aux | grep "
alias psr='ps aux | grep ruby'

# Show human friendly numbers and colors
alias df='df -h'
alias du='du -h -d 2'

# Color listing files
alias ll="ls -lhF -G"
alias la="ls -lhaF -G"
alias lsd="ls -lhF -G | grep  --color=never '^d'"
alias ls="command ls --color=auto"
alias lsg='ll | grep'

# Color grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Creating compacted cd ..
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

alias ssh="TERM=xterm-256color ssh"

alias ontoy='hostname'
alias adopssbx="ssh uid1v-mrosas.ap.dev.lax.gnmedia.net"
alias atomicsbx="ssh uid1v-mrosas.ao.dev.lax.gnmedia.net"
alias locallogs="tail -f /app/log/*_log"
alias cron="ssh app1v-cron.ap.prd.lax.gnmedia.net"
alias cron2="ssh app2v-cron.ap.prd.lax.gnmedia.net"
alias adopsDBdev="mysql -udev -pJocigEdth1 -h sql1v-56-adops.ap.dev.lax.gnmedia.net adops2_0_production"
alias dartDBdev="mysql -udev -pJocigEdth1 -h sql1v-56-adops.ap.dev.lax.gnmedia.net dart_scheduler"
alias adopsDev="ssh app1v-adops.ap.dev.lax.gnmedia.net"


alias phpcs='$HOME/.composer/vendor/bin/phpcs'
alias phpcbf='$HOME/.composer/vendor/bin/phpcbf'
alias phpmd='$HOME/.vendor/phpmd/src/bin/phpmd'
#alias ..="cd .."
#alias ...="cd ../.."
#alias h="cd ~"
#alias c="clear"
#alias phpunit="vendor/bin/phpunit"
#alias phpspec="vendor/bin/phpspec"
#alias mro="php artisan migrate:rollback"
#alias mig="php artisan migrate"
#alias dbs="php artisan db:seed"
#alias cacheclear="php artisan cache:clear"
#alias viewclear="php artisan view:clear"
#alias clearcompiled="php artisan clear-compiled"
#alias cda="composer dump-autoload"
#alias refresh="php artisan migrate:refresh --seed"
#alias openmysql="mysql -u homestead -psecret"
#alias art="php artisan"
#alias tinker="php artisan tinker"
#alias routes="php artisan route:list"
#alias adops_aglio="aglio -i docs/endpoints.apib --theme-template triple -o docs/endpoints.html"
#alias blueprint_adops="php artisan api:docs --name \"Adops API V1\" --output-file docs/endpoints.apib"
#
# alias for atomic
alias wp="cd /app/shared/wordpress/current"
# watching compass projects
alias compass-cr="wp && cd tools/compass && compass watch -c craveonline.rb"
alias compass-cr-m="wp && cd tools/compass && compass watch -c craveonline-mobile.rb"
alias compass-hf="wp && cd tools/compass && compass watch -c hockeysfuture.rb"
alias compass-psls="wp && cd tools/compass && compass watch -c playstationlifestyle.rb"
alias compass-rt="wp && cd tools/compass && compass watch -c realitytea.rb"
alias compass-th="wp && cd tools/compass && compass watch -c totallyher.rb"
alias compass-tfs="wp && cd tools/compass && compass watch -c thefashionspot.rb"
alias compass-mt="wp && cd tools/compass && compass watch -c momtastic.rb"
alias compass-st="wp && cd tools/compass && compass watch -c shocktillyoudrop.rb"
alias compass-idly="wp && cd tools/compass && compass watch -c idontlikeyouinthatway.rb"
alias compass-shh="wp && cd tools/compass && compass watch -c superherohype.rb"
alias compass-thm="wp && cd tools/compass && compass watch -c totallyhermedia.rb"
alias compass-we="wp && cd tools/compass && compass watch -c webecoist.rb"
alias compass-ele="wp && cd tools/compass && compass watch -c pb-elevate.rb"
alias compass-wz="wp && cd tools/compass && compass watch -c wrestlezone.rb"
alias compass-cs="wp && cd tools/compass && compass watch -c comingsoon.rb"
alias compass-lod="wp && cd tools/compass && compass watch -c liveoutdoors.rb"
alias compass-lo15="wp && cd tools/compass && compass watch -c lo-2015.rb"
alias compass-ae="wp && cd tools/compass && compass watch -c afterellen.rb"
alias compass-ss="wp && cd tools/compass && compass watch -c pb-slideshow.rb"
alias compass-tk="wp && cd tools/compass && compass watch -c totallykidz.rb"
alias compass-mtr="wp && cd tools/compass && compass watch -c momtastic-responsive.rb"
alias compass-base="wp && cd tools/compass && compass watch -c base.rb"
alias compass-wbf="wp && cd tools/compass && compass watch -c wholesomebabyfood.rb"
alias compass-ev="wp && cd tools/compass && compass watch -c evolvemedia.rb"
alias compass-br="wp && cd tools/compass && compass watch -c beautyriot.rb"
alias compass-crs="wp && cd tools/compass && compass watch -c crave-style.rb"
alias compass-dt="wp && cd tools/compass && compass watch -c dogtime.rb"
alias compass-ct="wp && cd tools/compass && compass watch -c cattime.rb"
alias compass-baser="wp && cd tools/compass && compass watch -c base-responsive.rb"
alias compass-mf="wp && cd tools/compass && compass watch -c musicfeeds.rb"
alias compass-rtv="wp && cd tools/compass && compass watch -c ringtv.rb"
alias compass-pbev="wp && cd tools/compass && compass watch -c pb-elevate-v2.rb"
alias compass-tba="wp && cd tools/compass && compass watch -c awards-totalbeauty.rb"
alias compass-tfsn="wp && cd tools/compass && compass watch -c tfs.rb"
alias compass-tfsb="wp && cd tools/compass && compass watch -c tfs-b.rb"
alias compass-gr="wp && cd tools/compass && compass watch -c gamerevolution.rb"
alias compass-gr-amp="wp && cd tools/compass && compass watch -c gamerevolution-amp.rb"
alias compass-pbpn="wp && cd tools/compass && compass watch -c pb-pinboard.rb"
alias compass-my="wp && cd tools/compass && compass watch -c mandatory.rb"
alias compass-ma18="wp && cd tools/compass && compass watch -c mandatory-2018.rb"
