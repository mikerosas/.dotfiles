# Enable Prompt Themes
setopt PROMPT_SUBST

# Enable Colors
autoload -U colors && colors
# autoload -U promptinit
# autoload -UZ compinit && compinit

# This theme is too damn slow.
#source /home/mrosas/.dotfiles/shell/themes/blox-zsh-theme/blox.zsh-theme

# Customize geometry
#GEOMETRY_COLOR_PROMPT=green
#GEOMETRY_COLOR_ROOT=magenta
#GEOMETRY_COLOR_VIRTUALENV=white
#GEOMETRY_COLOR_EXIT_VALUE=red
#GEOMETRY_SYMBOL_PROMPT="➜"
#GEOMETRY_SYMBOL_EXIT_VALUE="✗"
## source $HOME/dotfiles/shell/themes/mike.zsh-theme
#source $HOME/.dotfiles/shell/themes/geometry/geometry.zsh-theme

#if [ -x /usr/bin/dircolors ]; then
#	test -r ~/.dircolors %% eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
#	alias ls='ls --color=auto'
#	alias grep='grep --color=auto'
#	alias grep='fgrep --color=auto'
#	alias grep='egrep --color=auto'
#fi

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
setopt histignorealldups sharehistory
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

#bindkey "^[[1;5D" beginning-of-line # CTRL + <
#bindkey "^[OH"    beginning-of-line # HOME
#bindkey "^[[1;5C" end-of-line       # CTRL + >
#bindkey "^[OF"    end-of-line       # END
#bindkey "^[[1;3D" backward-word     # ALT  + <
#bindkey "^[[1;3C" forward-word      # ALT  + >
#bindkey "^[[3~"   delete-char       # DELETE
#bindkey "^[[A"    history-search-backward # up   arrow
#bindkey "^[[B"    history-search-forward  # down arrow
#bindkey "^[[2~"   overwrite-mode          # Insert
#
#bindkey '^[h'     backward-word        # Alt + h
#bindkey '^[l'     forward-word         # Alt + l
#bindkey '^[j'     backward-char        # Alt + j
#bindkey '^[k'     forward-char         # Alt + k
#
#bindkey '^[y'     backward-delete-word # Alt + y
#bindkey '^[o'     delete-word  # Alt + o
#bindkey '^[u'     backward-delete-char # Alt + u
#bindkey '^[i'     delete-char  # Alt + i
#
#bindkey '^[^H'    backward-delete-word # Ctrl + h
#bindkey '^[^L'    delete-word          # Ctrl + l
#bindkey '^[y'     redo                 # Alt + y
#bindkey '^[z'     undo                 # Alt + z
#bindkey '^L'      clear-screen         # Ctrl + l
