if empty(glob('$HOME/.vim/autoload/plug.vim'))
  silent !curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"REQUIRED
call plug#begin('$HOME/.vim/plugged')
"============
" Vim-Plug Packages
"============
Plug 'tpope/vim-sensible'
Plug 'jiangmiao/auto-pairs'
Plug 'mattn/emmet-vim'
Plug 'hzchirs/vim-material'
let Theme = "Material"
call plug#end()
"==========================="
" Documentations for plugins"
"==========================="
" * emmet-vim
"https://docs.emmet.io/
"https://www.youtube.com/watch?v=5BIAdWNcr8Y


"============
" Basic Setup
"============

" Fix backspace indent
set backspace=indent,eol,start

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Directories for swp files
set nobackup
set noswapfile
set showcmd
set showmatch
set noerrorbells
set autoread
"================
" Visual Settings
"================

syntax on
set cursorline
set number
set numberwidth=5
set relativenumber

" Color Theme
set t_Co=256
"set background=dark
"colorscheme vim-material

"There is love for italic comments
highlight Comment cterm=italic    
" Make it obviou where  80 characters is
set textwidth=80
"set colorcolumn=81
" Hightlight problematic whitespaces
set list listchars=tab:›\ ,trail:•,extends:»,precedes:«,nbsp:⣿
set gdefault
" Don't syntax highlight long lines
set synmaxcol=512
" Use one space, not two, after punctuation.
set nojoinspaces

" Status bar
set laststatus=2

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

map f !python -m json.tool<CR>              " format JSON
map l !xmllint --format --recover -<CR>     " format XML
