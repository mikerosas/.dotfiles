# Load custom variables on top.
source $HOME/.dotfiles/shell/env.sh

# Load prompt settings.
source $HOME/.dotfiles/shell/prompt.zsh

# Init antibody
source $HOME/.dotfiles/antibody/init

# Load Aliases.
source $HOME/.dotfiles/shell/aliases.zsh

# Load Completions
source $HOME/.dotfiles/shell/completions.zsh

# Break stuff here:
#
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

# Use base16
#BASE16_SHELL=/home/mrosas/.config/base16-shell/
#[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

# Use fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# # # Load rbenv
# eval "$(rbenv init -)"
#
# # # Load direnv
# eval "$(direnv hook zsh)"

function homestead() {
   ( cd ~/Homestead && vagrant $* )
}

