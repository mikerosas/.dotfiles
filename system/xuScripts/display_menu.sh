#!/bin/bash
ans=$(zenity --list --text "Display Settings" \
--radiolist --column "Pick" --column "Output Type" \
TRUE "Computer Only" \
FALSE Extended \
FALSE Docked \
);
DOCK_LEFT="DP-1-1"
DOCK_RIGHT="DP-1-2"
PROJECTOR="HDMI-1"
LAPTOP="eDP-1"

if [ "$ans" == "Computer Only" ]; then
	xrandr --output $LAPTOP --off
	xrandr --output $DOCK_LEFT --off
	xrandr --output $DOCK_RIGHT --off
	xrandr --output $LAPTOP --auto --primary
elif [ "$ans" == "Extended" ]; then
	xrandr --output $LAPTOP --off
	xrandr --output $PROJECTOR --off
	xrandr --output $DOCK_LEFT --off
	xrandr --output $DOCK_RIGHT --off
	xrandr --output $LAPTOP --auto --primary
	external=""
	if [ $(xrandr | grep VGA1 | awk ' { print $2 } ') == "connected" ]; then
		external="VGA1"
	elif [ $(xrandr | grep HDMI-1-1 | awk ' { print $2 } ') == "connected" ]; then
		external="HDMI-1-1"
	fi
	if [[ $external ]]; then
		xrandr --output $external --auto --right-of LVDS1
	fi
elif [ "$ans" == "Docked" ]; then
	primary=0
	xrandr --output $LAPTOP --off
	xrandr --output $DOCK_LEFT --off
	xrandr --output $DOCK_RIGHT --off
	if [ $(xrandr | grep $DOCK_LEFT | awk ' { print $2 } ') == "connected" ]; then
		primary=1
		xrandr --output $DOCK_LEFT --auto --primary
	fi
	if [ $(xrandr | grep DP2 | awk ' { print $2 } ') == "connected" ]; then
		if [[ $primary -eq 1 ]]; then
			xrandr --output DP2 --auto --left-of HDMI3
		else
			xrandr --output DP2 --auto --primary
		fi
	fi
else
	exit 0
fi
xfce4-panel -r
