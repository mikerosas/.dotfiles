#!/bin/bash

DOCK_LEFT="DP1-1"
DOCK_RIGHT="DP1-2"
PROJECTOR="HDMI1"
LAPTOP="eDP1"

DOCK_LEFT_STATUS=$(xrandr | grep $DOCK_LEFT | awk ' { print $2 } ')
DOCK_RIGHT_STATUS=$(xrandr | grep $DOCK_RIGHT | awk ' { print $2 } ')
PROJECTOR_STATUS=$(xrandr | grep $PROJECTOR | awk ' { print $2 }')

sleep 2

xrandr --output $LAPTOP --auto --primary

if [ "$DOCK_LEFT_STATUS" == "connected" ] && [ "$DOCK_RIGHT_STATUS" == "connected" ]; then
	xrandr --output $DOCK_LEFT --off
	xrandr --output $DOCK_RIGHT --off
	xrandr --output $LAPTOP --auto --primary
	xrandr --output $DOCK_LEFT --auto --right-of $LAPTOP
	xrandr --output $DOCK_RIGHT --auto --right-of $DOCK_LEFT
fi

if [ "$PROJECTOR" == "connected" ]; then
	xrandr --output $PROJECTOR --off
	xrandr --output $LAPTOP --off
	xrandr --output $LAPTOP --auto --primary
	xrandr --output $PROJECTOR --auto --right-of $LAPTOP
fi
