#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

# Update Packages
sudo apt-get update
sudo apt-get install -f
sudo apt-get install git
sudo apt-get install curl
# Install and supercharge z shell
sudo apt-get install zsh -y
sudo chsh -s /usr/bin/zsh vagrant
cd ~
#rm -r ~/.dotfiles
#git clone https://mikerosas@bitbucket.org/mikerosas/.dotfiles.git
#cd $HOME/.dotfiles/shell/plugins/zsh-autosuggestions/
#git clone https://github.com/zsh-users/zsh-autosuggestions.git .
#cd $HOME/.dotfiles/shell/plugins/zsh-history-substring-search/
#git clone https://github.com/zsh-users/zsh-history-substring-search.git .
#cd $HOME/.dotfiles/shell/plugins/zsh-syntax-highlighting/
#git clone https://github.com/zsh-users/zsh-syntax-highlighting.git .
#cd $HOME/.dotfiles/shell/themes/geometry/
#git clone https://github.com/geometry-zsh/geometry.git .
curl -sL git.io/antibody | sh -s
cd $HOME/.dotfiles/fzf/
git clone --depth 1 https://github.com/junegunn/fzf.git .
cd ~
.dotfiles/fzf/install
rm -f ~/.zshrc
ln -s ~/.dotfiles/zshrc ~/.zshrc


# Supercharge Vim
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
rm -f ~/.vimrc
ln -s ~/.dotfiles/vimrc ~/.vimrc

#Symlinks
#ln -s /home/mrosas/.dotfiles/system/shorcuts/google-chrome.desktop /home/mrosas/.local/share/applications/google-chrome.desktop

#php stuff
sudo apt-get install php-xml php-mcrypt -y

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

#cd ~
#mkdir Utilities
#cd Utilities
#git clone -b master https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards.git wpcs
#phpcs --config-set installed_paths $HOME/Utilities/wpcs
#phpcs -i

#install ruby with RVM
#\curl -sSL https://get.rvm.io | bash
#rvm install 2.3.0
#install bundler
#gem install bundler
#install guard


# Add extra aliases to z shell
#echo 'alias ..="cd .."' >> /home/vagrant/.zshrc
#echo 'alias ...="cd ../.."' >> /home/vagrant/.zshrc
#echo 'alias h="cd ~"' >> /home/vagrant/.zshrc
#echo 'alias c="clear"' >> /home/vagrant/.zshrc
#echo 'alias'
#echo 'alias phpunit="vendor/bin/phpunit"' >> /home/vagrant/.zshrc
#echo 'alias phpspec="vendor/bin/phpspec"' >> /home/vagrant/.zshrc
#echo 'alias mro="php artisan migrate:rollback"' >> /home/vagrant/.zshrc
#echo 'alias mig="php artisan migrate"' >> /home/vagrant/.zshrc
#echo 'alias dbs="php artisan db:seed"' >> /home/vagrant/.zshrc
#echo 'alias cacheclear="php artisan cache:clear"' >> /home/vagrant/.zshrc
#echo 'alias viewclear="php artisan view:clear"' >> /home/vagrant/.zshrc
#echo 'alias clearcompiled="php artisan clear-compiled"' >> /home/vagrant/.zshrc
#echo 'alias cda="composer dump-autoload"' >> /home/vagrant/.zshrc
#echo 'alias refresh="php artisan migrate:refresh --seed"' >> /home/vagrant/.zshrc
#echo 'alias openmysql="mysql -u homestead -psecret"' >> /home/vagrant/.zshrc
#echo 'alias art="php artisan"' >> /home/vagrant/.zshrc
#echo 'alias tinker="php artisan tinker"' >> /home/vagrant/.zshrc
#echo 'alias routes="php artisan route:list"' >> /home/vagrant/.zshrc
#echo 'alias adops_aglio="aglio -i docs/endpoints.apib --theme-template triple -o docs/endpoints.html"' >> /home/vagrant/.zshrc
#echo 'alias blueprint_adops="php artisan api:docs --name \"Adops API V1\" --output-file docs/endpoints.apib"' >> /home/vagrant/.zshrc
